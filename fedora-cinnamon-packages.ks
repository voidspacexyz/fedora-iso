# fedora-cinnamon-packages.ks
#
# Description:
# - Fedora package set for the Cinnamon Desktop Environment
#
# Maintainer(s):
# - Dan Book <grinnz@grinnz.com>

%packages

@networkmanager-submodules
@cinnamon-desktop
@libreoffice

# internet and multimedia. Modified. Removed parole, since we are installing vlc
hexchat
# Replaced transmission with qbittorrent. This is a much better choice.
qbittorrent 

# make sure we have a graphical installer
#yumex-dnf

# extra backgrounds
#desktop-backgrounds-basic

# save some space
-PackageKit*                # we switched to yumex, so we don't need this

#FSMK Packages

#Common
git
nginx
vlc
gnome-software
cheese #Webcam
emacs #In alphabetical order, just making sure, no one wages a war
vim-common
vim-enhanced
meld  #code diff tool
boxes # For virtualization
tor
asciinema #Terminal session recorder. Useful for saving their terminal sessions and storing it for later reviews/learning
youtube-dl 
unzip
gcc 
gcc-c++
zlib-devel
make 
autoconf 
autogen 
automake
fpaste 
Zim #Note taking app. Really an awesome one. 
SDL
os-prober 
powertop 
acpi
tmux 
weechat 
bitlbee
sqlite # Should be there by default. Just being explict
geany

#hardware
kicad
fritzing
arduino
arduino-core

#frontend
npm
nodejs

#backend
python-devel
python-virtualenv
python-pip
python-flask
python-flask-login
python-flask-sqlalchemy
python-flask-debugtoolbar
mariadb
mongodb
pymongo
uwsgi

#animation
gimp
inkscape
blender


%end
