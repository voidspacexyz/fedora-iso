# Include the appropriate repo definitions

# Exactly one of the following should be uncommented

# For the master branch the following should be uncommented
# %include fedora-repo-rawhide.ks

# For non-master branches the following should be uncommented
%include fedora-repo-not-rawhide.ks

# Adding RPMFusion Free to the list
repo --name="RPMFusion Free" --baseurl=http://download1.rpmfusion.org/free/fedora/releases/$releasever/Everything/$basearch/os/ --cost=1000
repo --name="RPMFusion Free - Updates" --baseurl=http://download1.rpmfusion.org/free/fedora/updates/$releasever/$basearch/ --cost=1000
